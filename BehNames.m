function [name_tree, vidList2] = BehNames(dir2choose,path)

folder_list=dir(dir2choose);
temp=zeros(length(folder_list)-2,8);
%�� ������ ������ � ���������� ���������� �������� ��������� ���������

for i=3:length(folder_list)
    file_name=folder_list(i).name;
    %��������� �����������
    unders=find(file_name=='_')';
    unders=[unders(1:min([3, length(unders)])) ones(min([3, length(unders)]),1)];
    ticks=[find(file_name=='-')' ones(length(find(file_name=='-')),1)*2];
    if size(unders,1)>2
        all_markers=sortrows([unders;ticks]);
    elseif length(unders)==2
        dots=[find(file_name=='.',1,'first')' 3];
        all_markers=sortrows([unders; ticks; dots]);
    end
    
    for i1=1:length(all_markers)-1
        temp(i-2,i1)=str2double(file_name(all_markers(i1,1)+1:all_markers(i1+1,1)-1));
    end
    
    temp(i-2,7)=length(find(file_name=='.'));
end

iter=0;
i=1;
while i<size(temp,1)+1
    iter=iter+1;
    unique_lines(iter)=i;
    if temp(i,7)>1
        i1=i+1;
        while (temp(i1,1)==temp(i,1)&&temp(i1,2)==temp(i,2)&&temp(i1,3)==temp(i,3)&&...
                temp(i1,4)==temp(i,4)&&temp(i1,5)==temp(i,5))&&i1<size(temp,1)
            i1=i1+1;
        end
        if i1==size(temp,1)
            i1=i1+1;
        end
        temp(i,8)=i1-i;
        i=i1;
    else
        temp(i,8)=1;
        i=i+1;
    end
end
temp=temp(unique_lines,:);

%������������ �������, ���������� ����� ��� ���� ��������������� ������
for i=1:size(temp,1);
    name_tree(i).monk=path.monkey;
    name_tree(i).date=temp(i,1:6);
    name_tree(i).quant=temp(i,8);
    
    file_name=folder_list(unique_lines(i)+2).name;
    unders=find(file_name=='_');
    dots=find(file_name=='.');
    
    %�������� ����� ����� ������ ��� �������
    if name_tree(i).quant>1
        for i1=1:name_tree(i).quant
            name_tree(i).vid(i1,:)=[file_name(1:dots(1)-1) '.' num2str(i1) '.avi'];
            temp_vL(i1)={name_tree(i).vid(i1,:)};
        end
    else
        name_tree(i).vid=file_name;
        temp_vL(1)={name_tree(i).vid};
    end
    vidList2(i)={temp_vL};
    clear temp_vL
    
    month_insert=['0' num2str(name_tree(i).date(2))];
    day_insert=['0' num2str(name_tree(i).date(3))];
    %�������� ��� prt �����
    name_tree(i).prt=[path.prt '\' num2str(name_tree(i).date(1)) '\'...
        name_tree(i).monk 'TR' month_insert(end-1:end) day_insert(end-1:end) '.PRT'];
    
    %�������� ��� ����� results diodepos diodelum � behdatacle
    name_tree(i).txt=[path.txt '\' '*Results' file_name(unders(1):unders(2)+5) '*'];
    
    name_tree(i).dpos=[path.jpg '\' 'Diodepos' file_name(unders(1):dots(1)-1) '.mat'];
    
    name_tree(i).dpos_jpg=[path.jpg '\' 'Diodepos' file_name(unders(1):dots(1)-1) '.jpg'];
    
    name_tree(i).dlum=[path.lum '\' 'DiodeLum' file_name(unders(1):dots(1)-1) '.mat'];
    
    name_tree(i).res=[path.res '\' 'BehData' file_name(unders(1):dots(1)-1) '.mat'];
    
    name_tree(i).xls=[path.xls '\' 'SaccadeData' file_name(unders(1):dots(1)-1) '.xls'];
end