function [diode1num, diode2num, diode1info, diode2info] = getDiodeOnOff(filename)
% �������� ������� ��������� � ���������� ������ �� ������ DiodeLum

%path = setPath;
load(filename);

setTreshold1 = min(diode1lum) + (max(diode1lum) - min(diode1lum))/2;%quantile(diode1lum,.70); %max(diode1lum) * .85;
setTreshold2 = min(diode2lum) + (max(diode2lum) - min(diode2lum))/2;%quantile(diode2lum,.70); %max(diode2lum) * .85;

diode1on = find(diode1lum>setTreshold1);%������� �������, ����� ����� ������ ����
diode1diff = diff(diode1on);%������� �������� ������� ����� �����, ����� ����� ������ �������

% ����� ������� "��������" ������ ����� 1, ��������
d1_not1 = find(diode1diff>1);
d1_not1diff = diff(d1_not1);
spikeId = find(d1_not1diff == 1);
if ~isempty(spikeId)
    for ii = 1:length(spikeId)
        currId = d1_not1(spikeId(ii));
        diode1on(currId+1) = diode1on(currId);
    end
    diode1on = unique(diode1on);
    diode1diff = diff(diode1on);
else
    disp('"��������" ������� ����� 1 �����������')
end
clear spikeId

tmp = diode1diff;
tmp(tmp == 1) = [];
gapLen = min(tmp) / 2;

diode1onId = find(diode1diff>gapLen) + 1; %����� ������� ��������, ��� ������ �������
diode1onTime = diode1on([1; diode1onId]); %����� ������� ������ �������

diode1offId = find(diode1diff>gapLen);
diode1offTime = diode1on([diode1offId; end]);

% ���������, ��� �� ��������� ������ �������
if length(diode1onId) ~= length(diode1offId)
    fprintf(2,'����� ��������� 1�� ����� �� ����� ����� ���������� !!');
end
disp('����� ��������� 1�� ����� ����� ����� ����������')

diode2on = find(diode2lum>setTreshold2);
diode2diff = diff(diode2on);

% ����� ������� "��������" ������ ����� 2, ��������
d2_not1 = find(diode2diff>1);
d2_not1diff = diff(d2_not1);
spikeId = find(d2_not1diff == 1);
if ~isempty(spikeId)
    for ii = 1:length(spikeId)
        currId = d2_not1(spikeId(ii));
        diode2on(currId+1) = diode2on(currId);
    end
    diode2on = unique(diode2on);
    diode2diff = diff(diode2on);
else
    disp('"��������" ������� ����� 2 �����������')
end
clear spikeId

diode2onId = find(diode2diff>gapLen)+1;
diode2onTime = diode2on([1; diode2onId]);

diode2offId = find(diode2diff>gapLen);
diode2offTime = diode2on([diode2offId; end]);

if length(diode2onId) ~= length(diode2offId)
    fprintf(2,'����� ��������� 2�� ����� �� ����� ����� ���������� !!\n');
    error('�� ������� �������� ��� ��������� � ���������� ������ �� ����� !!\n')
end
disp('����� ��������� 2�� ����� ����� ����� ����������')

if length(diode2onId) > length(diode1onId)
    fprintf(2,'����� ��������� 2�� ����� ������ ����� ��������� 1�� �����, \n������ �����, ��� ����������.\n');
end

diode1num = length(diode1onTime);
diode2num = length(diode2onTime);

diode1info = [diode1onTime diode1offTime];
diode2info = [diode2onTime diode2offTime];

% slashId = find(filename == filesep, 1, 'last');
% savename = [path.times filesep 'DiodeTimes' filename(slashId+9:end)];
% save(savename,'diode1onTime','diode2onTime')

end