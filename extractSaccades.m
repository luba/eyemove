function [behData] = extractSaccades(results, sortedDb, aDat, ...
    diode1lum, diode2lum, A, B, E, F)
% A - ��������� ����� �����
% B - ����� ��������� ����� � �������������

% ��������� ������� ������ �� ��� � �������
B = B / 10^6;

% ����� ������: �������� ����� min � max
setTreshold1 = min(diode1lum) + (max(diode1lum) - min(diode1lum))/2;
setTreshold2 = min(diode2lum) + (max(diode2lum) - min(diode2lum))/2;

% ���� 1
diode1on = find(diode1lum>setTreshold1);%������� �������, ����� ����� ������ ����
diode1diff = diff(diode1on);%������� �������� ������� ����� �����, ����� ����� ������ �������

% ����� ������� "��������" ������ ����� 1, ��������
d1_not1 = find(diode1diff>1);
d1_not1diff = diff(d1_not1);
spikeId = find(d1_not1diff == 1);
if ~isempty(spikeId)
    for ii = 1:length(spikeId)
        currId = d1_not1(spikeId(ii));
        diode1on(currId+1) = diode1on(currId);
    end
    diode1on = unique(diode1on);
    diode1diff = diff(diode1on);
else
    disp('"��������" ������� ����� 1 �����������')
end
clear spikeId

% ����� ��� �������� ����� ����������� ����� 1; ��� ������ 1, ��� ��������
tmp = diode1diff;
tmp(tmp == 1) = [];
gapLen = min(tmp) / 2;

diode1onId = find(diode1diff>gapLen) + 1; %����� ������� ��������, ��� ������ �������
diode1onTime = diode1on([1; diode1onId]); %����� ������ ����� ������ �������
%diode1onTS =  % ������� ��������� ������

diode1offId = find(diode1diff>gapLen);
diode1offTime = diode1on([diode1offId; end]);

% ���������, ��� �� ��������� ������ �������
if length(diode1onId) ~= length(diode1offId)
    fprintf(2,'����� ��������� 1�� ����� �� ����� ����� ���������� !!');
end
disp('����� ��������� 1�� ����� ����� ����� ����������')

diode2on = find(diode2lum>setTreshold2);
diode2diff = diff(diode2on);

% ����� ������� "��������" ������ ����� 2, ��������
d2_not1 = find(diode2diff>1);
d2_not1diff = diff(d2_not1);
spikeId = find(d2_not1diff == 1);
if ~isempty(spikeId)
    for ii = 1:length(spikeId)
        currId = d2_not1(spikeId(ii));
        diode2on(currId+1) = diode2on(currId);
    end
    diode2on = unique(diode2on);
    diode2diff = diff(diode2on);
else
    disp('"��������" ������� ����� 2 �����������')
end
clear spikeId

diode2onId = find(diode2diff>gapLen)+1;
diode2onTime = diode2on([1; diode2onId]);

diode2offId = find(diode2diff>gapLen);
diode2offTime = diode2on([diode2offId; end]);

if length(diode2onId) ~= length(diode2offId)
    fprintf(2,'����� ��������� 2�� ����� �� ����� ����� ���������� !!\n');
    error('�� ������� �������� ��� ��������� � ���������� ������ �� ����� !!\n')
end
disp('����� ��������� 2�� ����� ����� ����� ����������')

if length(diode2onId) > length(diode1onId)
    fprintf(2,'����� ��������� 2�� ����� ������ ����� ��������� 1�� �����, \n������ �����, ��� ����������.\n');
end

% ��������� ������� ��������� ������� ������, ���������� ������
figure(248),
subplot(2,1,1) % ���� 1
plot(diode1lum)
hold on
plot([1 numel(diode1lum)], [setTreshold1 setTreshold1])
hold off

subplot(2,1,2) % ���� 2
plot(diode2lum)
hold on
plot([1 numel(diode2lum)], [setTreshold2 setTreshold2])
hold off

% ���������� �������� ���� ��������
figure(249),
subplot(2,1,1) % 1 ����
plot(B,E)

subplot(2,1,2) % ������ ����
plot(B,F)

% ������� ��������� � ���������� ������, ���������� �������� �����
figure(250),
scatter(B(diode1on), 400*ones(numel(diode1on),1),'r.')
hold on
% ������� ������ ������� �� ������ ����� � ������ ��������� ������� �����
for ii = 1:length(diode1onTime)
    text(B(diode1onTime(ii)),411, num2str(ii))
end
scatter(B(diode2on), 405*ones(numel(diode2on),1),'b.')
legend('������ ����', '������ ����')

plot(B, E, 'k') % �������� ����� � �������������� ���������

plot(B(diode1onTime),410*ones(length(diode1onTime),1),'r.')
plot(B(diode2onTime),411*ones(length(diode2onTime),1),'b.')

plot(B(diode1offTime),410*ones(length(diode1offTime),1),'g.')
plot(B(diode2offTime),411*ones(length(diode2offTime),1),'k.')
hold off

ylim([0 450])

unqResp = unique(results.rDataResp);

% ������ � ������� �� ����� PRT
corrResp = strfind(results.rDataResp,'CORRECT'); % ����� ���������� �������
incorrResp = cellfun(@isempty,corrResp); % ����� ������������ �������
fixpointResp = strfind(results.rDataTasks,'Fix.Point');
fixpointResp = cellfun(@isempty,fixpointResp);
fixpointResp = ~fixpointResp;
corrId = ~incorrResp;
corrId = find(corrId); % ����� ������ ���������� �������
% �������, ����� ��������� f.p. - ������ �������
FPResp = strfind(results.rDataResp,'F.p.');
FPResp = ~cellfun(@isempty, FPResp);
% �������, ����� ��������� gaP - ������ ������� (������ ������� �� f.p.)
gapResp = strfind(results.rDataResp,'gaP');
gapResp = ~cellfun(@isempty, gapResp);
% �������, ����� �� Fix.Point � ��� ���� �� f.p., � �� gaP - �������� ��� �����
twoDids = ~fixpointResp & ~FPResp & ~gapResp;

clear corrResp incorrResp

timePre = 0.2;% seconds
timePost = 2.5;% seconds

% �������� ������ �� ���������� ������ � ���������
for trNum = 1:length(diode1onTime)
    % ����� ��������� ������� ����� - ����������� ������������ �����
    diodeData(trNum).diode1on = B(diode1onTime(trNum)); % ����� ��������� ����� �� �����, ������� � ������� �� ���
    % ����� ���������� ������� ����� - ����������� ������������ �����
    diodeData(trNum).diode1off = B(diode1offTime(trNum)); % ����� ��������� ����� �� �����, ������� � ������� �� ���
    
    % ���, ����� ��������� 2 ���� � ���� �������
    if trNum == length(diode1onTime)
        % ���� �������������� ������� - ��������� � ������������
        d2id = find(diode2onTime>diode1onTime(trNum));
    else
        % ��� ���� ��������� �������
        d2id = find(diode2onTime>diode1onTime(trNum) & ...
            diode2onTime<diode1onTime(trNum + 1));
    end
    
    if length(d2id) == 1%~isempty(d2id)
        diodeData(trNum).diode2on = B(diode2onTime(d2id));
        diodeData(trNum).diode2off = B(diode2offTime(d2id));
    elseif isempty(d2id)
        diodeData(trNum).diode2on = 0;
        diodeData(trNum).diode2off = 0;
    else
        error('��� ��������� ����� 2 �� �������. ��-��!!')
    end
end

% ���������, ������ �� ����� ������� � ������ ����� � ����� PRT, ���
% ����������
if length(diodeData) ~= length(results.rDataResp)
    d = abs(length(results.rDataResp) - length(diodeData));
    fprintf(2,'����� ������� � ����� � � PRT ����� �� �������� �� %d!!\n',d);
end

% ���� ��� ������ 20 ������� ��������� � ������ ����� � prt-�����,
% ��������� ��������� ����� �������
if isequal(double([diodeData(1:20).diode2on]==0), fixpointResp(1:20))
    % � ����� ��������, ���� �� ������������� ����� ��������������������
    dataLen = min(length(diodeData),min(fixpointResp));
    a = zeros(dataLen, 1);
    for ii = 1:dataLen
        a(ii) = isequal([diodeData(1:ii).diode2on]==0, ~twoDids(1:ii));
    end
    mis = find(a==0,1,'first'); % ����� ������� � ������ ���������������
    if isempty(mis)
        % ��� ������, ������������ ������ �� ���� �����
        disp('������������������ ������� � ����� � prt ��������� �� ���� �����')
    else
        % ������������ ������ ������ � ���������, ����� ������������������
        % ���������
        fprintf(2, '��������� ������������������ ������ �� %d �������', mis - 1);
        dataLen = mis - 1;
    end
else
    fprintf(2, '�� ������� ���������� ������������ ������');
    behData = [];
    return
end
    
corrId(corrId > length(diode1onTime)) = [];

% % ���������, ������ �� ����� ������� � ������ ����� � ����� PRT
% if length(diodeData) ~= length(results.rDataResp)
%     d = abs(length(results.rDataResp) - length(diodeData));
%     fprintf(2,'����� ������� � ����� � � PRT ����� �� �������� �� %d!!\n',d);
%     % ���� ���, ������� ����� �� �������
%     if length(diodeData) < length(results.rDataResp)
%         fragment = double([diodeData(1:20).diode2on]==0);
%         for ii = 1:length(fixpointResp)-length(fragment)
%             e(ii) = isequal(fragment,fixpointResp(ii:ii+length(fragment)-1));
%         end
%     else
%         fragment = fixpointResp(1:20);
%         for ii = 1:length([diodeData.diode2on])-length(fragment)
%             e(ii) = isequal(fragment, [diodeData(ii:ii+length(fragment)-1).diode2on]==0);
%         end
%     end
%     
%     if isempty(find(e))
%         fprintf(2,'�� ������� ���������� ������������ ������\n');
%         behData = [];
%         return
%     end
% 	lag = find(e) - 1;
% 	corrId(corrId > length(diode1onTime)) = [];
% elseif length(find([diodeData.diode2on])) ~= sum(twoDids)
%     fprintf(2,'������� �� ��� ��������� ������� �����\n');
% else
%     disp('����� ������� � ����� � � PRT ����� �����')
% end
% end

% �������� ������ �� ���������� �������� � ���������
for trNum = 1:numel(corrId)
    % ���������� ����� �������
    behData(trNum).trial = corrId(trNum);
    % ��� ������ � �������
    behData(trNum).task = results.rDataTasks{corrId(trNum)};
    % ����� ��������� ������� ����� - ����������� ������������ �����
    behData(trNum).diode1on = diodeData(corrId(trNum)).diode1on;
    % ����� ���������� ������� ����� - ����������� ������������ �����
    behData(trNum).diode1off = diodeData(corrId(trNum)).diode1off;
    % ����� ��������� ������� ����� - ��������������� �������
    behData(trNum).diode2on = diodeData(corrId(trNum)).diode2on;
    % ����� ���������� ������� ����� - ��������������� �������
    behData(trNum).diode2off = diodeData(corrId(trNum)).diode2off;
    % ������ ����� ������������ �������, ������� ����� ����� ��� �������
    % ������
    selList = B > (behData(trNum).diode1on - timePre) & ...
        B < (behData(trNum).diode1on + timePost);
    % ����� �������� �����
    behData(trNum).time = B(selList);
    % ���������� ������ �� ��� X
    behData(trNum).saccadeX = E(selList);
    % ���������� ������ �� ��� Y
    behData(trNum).saccadeY = F(selList);
    % ������� ���� ����� ������ ���������
    selLateFix = behData(trNum).time > behData(trNum).diode1off - 0.1 & ...
        behData(trNum).time < behData(trNum).diode1off;
    % ������������� ���. ������������ ������������ ������� ����� 
    % ����������� ��������������� �������
    behData(trNum).fixCntrdSaccX = behData(trNum).saccadeX - ...
        mean(behData(trNum).saccadeX(selLateFix));
    % ������������� ����. ������������ ������������ ������� ����� 
    % ����������� ��������������� �������
    behData(trNum).fixCntrdSaccY = behData(trNum).saccadeY - ...
        mean(behData(trNum).saccadeY(selLateFix));
    % ���������� ������������� ���� � ���������
    behData(trNum).behCodes = aDat.data(corrId(trNum),5:7);
    % ���������� ������
    behData(trNum).target = aDat.data(corrId(trNum),5:6);
end

if length(behData)>=100
    range=76:100;
    minus=75;
else
    range=1:24;
    minus=0;
end

figure(251)
for ii = range
    subplot(5,5,ii-minus)
    hold on
    
    fill([behData(ii).diode1on behData(ii).diode1on...
        behData(ii).diode1off behData(ii).diode1off],...
        [-50 50 50 -50], 'r', 'EdgeColor', 'none', ...
        'FaceAlpha',0.3)
    
    if behData(ii).diode2on > 0
        fill([behData(ii).diode2on(1) behData(ii).diode2on(1) ...
            behData(ii).diode2off(1) behData(ii).diode2off(1)],...
            [-50 50 50 -50], 'b', 'EdgeColor', 'none', ...
            'FaceAlpha',0.3)
    end
    
    selLateFix = behData(ii).time > behData(ii).diode1off - 0.1 & behData(ii).time < behData(ii).diode1off;
    plot(behData(ii).time,behData(ii).saccadeX - mean(behData(ii).saccadeX(selLateFix)),'b');
    plot(behData(ii).time,behData(ii).saccadeY - mean(behData(ii).saccadeY(selLateFix)),'g');
    %     plot(behData(ii).time,behData(ii).saccadeX,'b');
    %     plot(behData(ii).time,behData(ii).saccadeY,'g');
    %    set(gca, 'Ylim', [-55 250]);
    axis tight
    hold off
    
    title(['Trial #' num2str(behData(ii).trial)])
    ylim([-50 50])
    
    %     figure(252)
    %     subplot(3,4,ii)
    %     hold on
    %
    %     fill([0.2 0.2 ...
    %         (behData(ii).diode1off - behData(ii).diode1on) ...
    %         (behData(ii).diode1off - behData(ii).diode1on)], ...
    %         ...
    %         [-40 40 40 -40], ...
    %         'r', ...
    %         'EdgeColor', 'none', ...
    %         'FaceAlpha',0.3)
    %
    %     fill([behData(ii).diode2on - behData(ii).diode1on ...
    %         behData(ii).diode2on - behData(ii).diode1on ...
    %         behData(ii).diode2off - behData(ii).diode1on ...
    %         behData(ii).diode2off - behData(ii).diode1on], ...
    %         [-40 40 40 -40],'b',...
    %         'EdgeColor', 'none', ...
    %         'FaceAlpha',0.3)
    %
    %     diffResp = abs(diff(behData(ii).saccadeX)).^2;
    %     outlierId = find(diffResp>200);
    %     %diffResp([outlierId-3:outlierId+3]) = 0;
    %     %bar((1:639)/frameRate,diffResp)
    %     bar(behData(ii).time(1:end-1),diffResp)
    %     xlim([0 3.5])
    %     %ylim([0 20])
end
    
figure(155); 
unqTargets = unique(cellfun(@num2str, {behData.target},'UniformOutput',0));
currClrMap = colormap('jet');

%������� ���������� �������� ������� ��� ������� ���������� �����(35 � ������ ���
%����� � 16 ��� �����)
multiplier=1:length(unqTargets);
multiplier=round(multiplier*(64/length(unqTargets)));

hold on 
nTrials = length(behData);
for ii =  1:nTrials
    if strcmp(behData(ii).task,'NoDelay')
        %currClrInd = find(strcmp(unqTargets, num2str(behData(ii).target)))*4;
        currClrInd = multiplier(find(strcmp(unqTargets, num2str(behData(ii).target))));
        selEndFix = behData(ii).time > behData(ii).diode1off;      
        plot(behData(ii).fixCntrdSaccX(selEndFix), behData(ii).fixCntrdSaccY(selEndFix), '.',...
            'MarkerFaceColor', currClrMap(currClrInd,:), ...
            'MarkerEdgeColor', currClrMap(currClrInd,:), 'MarkerSize', .5);
%         plot(behData(ii).saccadeX(selEndFix), behData(ii).saccadeY(selEndFix), '.',...
%             'MarkerFaceColor', currClrMap(currClrInd,:),...
%             'MarkerEdgeColor', currClrMap(currClrInd,:), 'MarkerSize', .5);
    end
end
hold off

figure(156); 
%unqTargets = unique(cellfun(@num2str, {behData.target},'UniformOutput',0));
currClrMap = colormap('jet');

hold on
nTrials = length(behData);
for ii =  1:nTrials
    currClrInd = multiplier(find(strcmp(unqTargets, num2str(behData(ii).target))));
    plot(behData(ii).saccadeX, behData(ii).saccadeY, '.',...
        'MarkerFaceColor', currClrMap(currClrInd,:),...
        'MarkerEdgeColor', currClrMap(currClrInd,:), 'MarkerSize', .5);
end
hold off
