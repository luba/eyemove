function BehNames_unified()
%�������� �������
%��������������� ��� ����� ������ �������� (avi, txt, mat) � ������������ �
%����� ��������

[path_file, path_folder]=uigetfile();
load([path_folder path_file])

global names_tr

fields_of_int=['avi'; 'txt'; 'lum'; 'jpg'; 'res'; 'xls'];

for i00=1:size(fields_of_int,1)
    %�������������� ���� ������
    Nm=fields_of_int(i00,:);
    fldr_name=path.(fields_of_int(i00,:));
    fldr_dir=dir(fldr_name);
    names_tr.names.(Nm)=zeros(1,6);
    for i=3:length(fldr_dir)
        i0=i-2;
        names_tr.(Nm)(i0).file_N=fldr_dir(i).name;
        %��������� �����������
        unders=find(names_tr.(Nm)(i0).file_N=='_')';
        unders=[unders(1:min([4, length(unders)])) ones(min([4, length(unders)]),1)];
        ticks=[find(names_tr.(Nm)(i0).file_N=='-')' ones(length(find(names_tr.(Nm)(i0).file_N=='-')),1)*2];
        
        if size(unders,1)>2
            all_markers=sortrows([unders; ticks]);
            delims=['_' '-'];
        elseif length(unders)==2
            dots=[find(names_tr.(Nm)(i0).file_N=='.',1,'first')' 3];
            all_markers=sortrows([unders; ticks; dots]);
            delims=['_' '-' '.'];
        end
        
        %�������� �� ��, ���� �� ������ ���������� ������
        ref_num2=names_tr.(Nm)(i0).file_N(all_markers(2,1)+1:all_markers(3,1)-1);
        
        %�������� �� ��, ���� �� ������ ��� ��� ����
        ref_num=names_tr.(Nm)(i0).file_N(all_markers(1,1)+1:all_markers(2,1)-1);
        
        %���������� ������ �����; ���� ������ �������� �� ���������������� ��
        %��� ���, �� ��� �����, ��� ����������� ��� ������
        if length(ref_num)==4
            %���� ������ �������� ���������������� ��� ���, ��� ���������
            %����������
            names_tr.(Nm)(i0).new_N=names_tr.(Nm)(i0).file_N;
            names_tr.(Nm)(i0).time_nums=zeros(length(all_markers)-1,1);
            for i1=1:length(all_markers)-1
                names_tr.(Nm)(i0).time_nums(i1)=str2double(names_tr.(Nm)(i0).file_N...
                    (all_markers(i1,1)+1:all_markers(i1+1,1)-1));
            end
        elseif length(ref_num)<3
            
            %���������� � ������, ���� ������ ���� ���������� ������
            if length(ref_num2)>4
                shift_num=2;
                dots=[find(names_tr.(Nm)(i0).file_N=='.',1,'first')' 3];
                all_markers=sortrows([unders; ticks; dots]);
                delims=['_' '-' '.'];
            else
                shift_num=0;
            end
            
            %���� ������ �������� �� ���������������� ��� ���, ��� ���������
            %������������ � ��������������
            names_tr.(Nm)(i0).new_N=names_tr.(Nm)(i0).file_N(1:all_markers(1,1)-1);
            names_tr.(Nm)(i0).time_nums=zeros(length(all_markers)-1,1);
            for i1=3+shift_num:-1:1+shift_num
                temp_num=['0' names_tr.(Nm)(i0).file_N(all_markers(i1,1)+1:all_markers(i1+1,1)-1)];
                if i1~=3+shift_num
                    names_tr.(Nm)(i0).new_N=[names_tr.(Nm)(i0).new_N delims(all_markers(4+shift_num-(i1-shift_num),2)) temp_num(end-1:end)];
                else
                    names_tr.(Nm)(i0).new_N=[names_tr.(Nm)(i0).new_N delims(all_markers(4+shift_num-(i1-shift_num),2)) temp_num(2:end)];
                end
                names_tr.(Nm)(i0).time_nums(4+shift_num-i1)=str2double(temp_num);
            end
            
            for i1=4+shift_num:length(all_markers)-1
                temp_num=['0' names_tr.(Nm)(i0).file_N(all_markers(i1,1)+1:all_markers(i1+1,1)-1)];
                names_tr.(Nm)(i0).new_N=[names_tr.(Nm)(i0).new_N delims(all_markers(i1,2)) temp_num(end-1:end)];
                names_tr.(Nm)(i0).time_nums(i1)=str2double(temp_num);
            end
            
            if i00==2
                names_tr.(Nm)(i0).new_N=names_tr.(Nm)(i0).new_N(1:end-3);
            end
            
            end_seq=names_tr.(Nm)(i0).file_N(all_markers(end,1):end);
            names_tr.(Nm)(i0).new_N=[names_tr.(Nm)(i0).new_N end_seq];
            
            %��������������� �������������� �����
            cd(fldr_name)
            movefile(names_tr.(Nm)(i0).file_N, names_tr.(Nm)(i0).new_N)
            cd('E:\eyemove\')
        elseif length(find(all_markers(:,2)==2))<3
            %���� ������ �������� �� ���������������� ��� ���, ��� ���������
            %�������������� � ������������� �� �����
            names_tr.(Nm)(i0).new_N=[];
        end
    end
   names_tr.names.(Nm)=names_tr.names.(Nm)(2:end,:);
end

save([path.mom '\' 'changed_names_' path.mom(find(path.mom=='\',1,'last')+1:end) '.mat'], 'names_tr')