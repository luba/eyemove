function [diode1lum,diode2lum] = getDiodeLum(vidList, pnts2proc,path)

% ���������� ������ Matlab
v = version('-release');

%resourse = findresource;
%matlabpool('open',resourse.ClusterSize)

diode1lum = [];
diode2lum = [];

tic
for flNum = 1:length(vidList)
    fprintf('\nProcessing file %s\n', vidList{flNum});
    % ���������� ����� ������ � ����� ��� ������ �� ������ Matlab
    if strcmp(v,'2011a') || strcmp(v,'2009a')
        movObj = mmreader([path.avi filesep vidList{flNum}]);
    elseif strcmp(v,'2014b')
        movObj = VideoReader([path.avi filesep vidList{flNum}]);
    else
        error('������ ������ Matlab �� ��������������')
    end
    nFrames = movObj.NumberOfFrames;
    
    hWbr = waitbar(0,'1', 'Name', 'Processing video...'); % ������ ��������
    [diode1lum_tmp,diode2lum_tmp] = deal(zeros(nFrames,1)); % �������� ����� ��� ���������� �������� �������
    for ii = 1:nFrames
        cdata = read(movObj,ii); % �������� ���� ���� �� �����
        cdata = cdata(:,:,1); % ��������� rgb ���� ���������, ����� ������ ����
%         if strcmp(v,'2011a') || strcmp(v,'2009a')
%             cdata = read(movObj,ii); % �������� ���� ���� �� �����
%             cdata = cdata(:,:,1); % ��������� rgb ���� ���������, ����� ������ ����
%         elseif strcmp(v,'2014b')
%             cdata = readFrame(movObj); % �������� ���� ���� �� �����
%             cdata = cdata(:,:,1); % ��������� rgb ���� ���������, ����� ������ ����
%         else
%             error('������ ������ Matlab �� ��������������')
%         end
        
        

        % ������������ �������� ������ ������, ��������� ������� �������
        diode1lum_tmp(ii) = mean2(cdata(pnts2proc.d1y, pnts2proc.d1x)); 
        diode2lum_tmp(ii) = mean2(cdata(pnts2proc.d2y, pnts2proc.d2x));
        waitbar(ii/nFrames, hWbr, sprintf('%1.1f percent done',ii/nFrames*100))
    end
    diode1lum = [diode1lum; diode1lum_tmp];
    diode2lum = [diode2lum; diode2lum_tmp];
    delete(hWbr)
end
elpsdTm = toc;
fprintf('\nExtraction done in %1.1f minutes\n', elpsdTm/60);
