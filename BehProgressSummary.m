function BehProgressSummary()

%������� ��������� ������� ����������� ������ ��� ������� ������������,
%������� Excel ���� � � ��� � ������� �������� � �������� ���������
%�������� ������������, ��� ������� ���� ��� ����������� ������ � ���
%������� ����� ������ �����������

global path
global summary

%�������� ���������� ����� path
temp_fldr='E:\data\';
[Fname, Ffldr]=uigetfile([temp_fldr, '*path.mat']);
load ([Ffldr, Fname])

%�������� ����� ��������� path, �� ������� ����������� ������� �����
field_names=[{'avi'};{'txt'};{'lum'};{'jpg'};{'res'};{'xls'};{'prt'}];

%������� 10, ������� ������������ ��� �������� �������������� ����
%������������ � ������� YYYYMMDDHHMM
time_code=[8; 6; 4; 2; 0];

%���������, � ������� ������������ ���������� ������� ��� ���������� ������
summary=zeros(1,length(field_names)+1);

for i=1:length(field_names)
    if isfield(path, cell2mat(field_names(i)))==1
        t_dir=dir(path.(cell2mat(field_names(i))));
        if i<=6
            for i1=3:length(t_dir)
                if t_dir(i1).isdir==0
                    t_name=t_dir(i1).name;
                    
                    %��������� ������� ���� (.avi, .txt, .mat)
                    unders=find(t_name=='_')';
                    %��������� ��� txt ������ � ������, ���� �� ���� � ��� ������ ���
                    %�������������� ���������� (��������� ������������ ��������
                    %����� ����� ����������� txt), �������� Results_2018-08-14_12-39.txt
                    if i==2
                        upLim=4;
                    else
                        upLim=3;
                    end
                    
                    %������������ ������ ���� ������������ �� ������ _ - � .
                    unders=[unders(1:min([upLim, length(unders)])) ones(min([upLim, length(unders)]),1)];
                    ticks=[find(t_name=='-')' ones(length(find(t_name=='-')),1)*2];
                    
                    if size(unders,1)>2
                        all_markers=sortrows([unders;ticks]);
                        fin_point=2;
                    elseif length(unders)==2
                        dots=[find(t_name=='.',1,'first')' 3];
                        all_markers=sortrows([unders; ticks; dots]);
                        if i==2
                            fin_point=1;
                        else
                            fin_point=2;
                        end
                    end
                    ref_num=t_name(all_markers(1,1)+1:all_markers(2,1)-1);
                    
                    %������������ ���� ����� (=��� ������������)
                    temp_P=0;
                    
                    %��������, �������� �� ������ ���������� ����� ��� �����
                    if length(ref_num)==4
                        for i2=1:length(all_markers)-fin_point
                            temp_P=temp_P+str2double(t_name(all_markers(i2,1)+1:all_markers(i2+1,1)-1))*10^time_code(i2);
                        end
                    elseif length(ref_num)<=2
                        for i2=3:-1:1
                            temp_P=temp_P+str2double(t_name(all_markers(i2,1)+1:all_markers(i2+1,1)-1))*10^time_code(i2);
                        end
                        for i2=4:length(all_markers)-fin_poit
                            temp_P=temp_P+str2double(t_name(all_markers(i2,1)+1:all_markers(i2+1,1)-1))*10^time_code(i2);
                        end
                    end
                    
                    %����� ����������� ���� � ������������ ����. ���� �� ���
                    %��������� ���, �� � ������, �������������� ������ ����������,
                    %�������� 1, ���� �� ���������, ����������� ��� ����
                    %������� � ����� �����
                    match=find(summary(:,1)==temp_P);
                    if isempty(match)==0
                        insert=summary(match, 2:end);
                        insert(i)=insert(i)+1;
                        summary(match, 2:end)=insert;
                    else
                        insert=zeros(1,length(field_names)+1);
                        insert(1)=temp_P;
                        insert(i+1)=insert(i+1)+1;
                        summary=[summary; insert];
                    end
                end
            end
        else
            %��������� .prt �����, ��� �������������� �� �������� �����,
            %��������� ���� ������� �� �������� �����
            for i1=3:length(t_dir)
                t_dir2=dir([path.(cell2mat(field_names(i))) '\' t_dir(i1).name]);
                year=str2double(t_dir(i1).name);
                for i2=3:length(t_dir2)
                    t_name=t_dir2(i2).name;
                    temp_P=str2double(t_name(find(t_name=='R',1,'first')+1:find(t_name=='.',1,'first')-1))...
                        *10^time_code(3)+year*10^time_code(1);
                    %�������� �� �������� ������� ���� ������������. ���
                    %��� prt ���� ���������� ����, � �� ���� � ��������, �
                    %������, ���� ��� ������ ��� ��������� ����� ������
                    %���� ������������, 1 prt ����� ������� �� ����������
                    %������ � ���� ����
                    matches=find((summary(:,1)-temp_P<10000)&(summary(:,1)-temp_P>1000));
                    if isempty(matches)==1
                        insert=zeros(1,length(field_names)+1);
                        insert(1)=temp_P;
                        insert(i+1)=insert(i+1)+1;
                        summary=[summary; insert];
                    else
                        for i3=1:length(matches)
                            insert=summary(matches(i3), 2:end);
                            insert(i)=insert(i)+(1/length(matches));
                            summary(matches(i3), 2:end)=insert;
                        end
                    end
                end
            end
        end
    else
        fprintf(['���� path ��������� �����������, �� ������� ���� ' cell2mat(field_name(i))])
    end
end

%���������� ����������������� ����� �� �����������
summary=sortrows(summary,1);

%���������� �� ����, ��� ������� ���������� ������ ����� ����� prt ������
nE_rows=find(summary(:,2)>1|summary(:,3)>1|...
    summary(:,4)>1|summary(:,5)>1|summary(:,6)>1);

%�������� cell ������� � ������������ ������
summary2save=num2cell(summary(nE_rows,2:end));

%unicode ������� ��� - - _ :
delimits=[39;45;45;95;58;39];

%���������� ������� ������� � ����������� ���� ������������ �� ������
%YYYY-MM-DD_HH:MM
summary2save1=num2str(floor(summary(nE_rows,1)/(10^time_code(1))));
for i=2:length(time_code)
    shift=floor(summary(nE_rows,1)/10^time_code(i-1))*10^time_code(i-1);
    insert=ones(length(nE_rows),1);
    insert2=num2str(insert*100+floor((summary(nE_rows,1)-shift)/(10^time_code(i))));
    summary2save1=[summary2save1, char(insert*delimits(i)),insert2(:,end-1:end)];
end
summary2save1=cellstr(summary2save1);

%�������� ������� ��� ����������
table2save=cell2table([summary2save1, summary2save], 'VariableNames',...
    {'date','avi','txt','lum','jpg','res','xls','prt'});

%���������� �������
tableName=[path.mom '\' path.mom(find(path.mom=='\',1,'last')+1:end) '_summary.xls'];
writetable(table2save, tableName)

%������������ ������� �� ����������
poss_columns=['A'; 'B'; 'C'; 'D'; 'E'; 'F'; 'G'; 'H'];

%����� - ������-������� ��� ������� � ������-������� ��� ����������
poss_colors=[34, 40];

%������������� ������� excel
Excel = actxserver('excel.application');
WB = Excel.Workbooks.Open(fullfile(tableName),0,false);
%������� ������� ���� � �������
WB.Worksheets.Item(1).Range('A1:H1').Interior.ColorIndex = 50;

for i=1:length(nE_rows)
    %����� �����
    temp_control=zeros(8,2);
    temp_control(1:8,1)=1:8;
    temp_control(summary(nE_rows(i),:)==0,2)=2;
    temp_control(summary(nE_rows(i),:)~=0,2)=1;
    
    %������� ����� � ������
    range=[poss_columns(1) num2str(i+1) ':'...
        poss_columns(1) num2str(i+1)];
    
    if isempty(find(temp_control(:,2)==2))==1
        WB.Worksheets.Item(1).Range(range).Interior.ColorIndex = 50;
        range=[poss_columns(2) num2str(i+1) ':'...
            poss_columns(end) num2str(i+1)];
        WB.Worksheets.Item(1).Range(range).Interior.ColorIndex = poss_colors(1);
    else
        WB.Worksheets.Item(1).Range(range).Interior.ColorIndex = 3;
        for i1=2:length(temp_control)
            range=[poss_columns(i1) num2str(i+1) ':'...
                poss_columns(i1) num2str(i+1)];
            WB.Worksheets.Item(1).Range(range).Interior.ColorIndex = poss_colors(temp_control(i1,2));
        end
    end
end
WB.Save();
WB.Close();
Excel.Quit();