function parseBehData

global name_tree

%�������� ��������� � ������� ����������
dpath = setPath;

[name_tree, vidList]=BehNames(dpath.avi,dpath);
BehTxtMerge(name_tree,dpath);

% �������� ������ ����� � ���������, ������� ����������� ���� ���������
% ������ �� ������������
pnts2proc = getDiodePos(vidList,dpath);

% ���������� ���������� ������ ��� ������� �����
for vidNum = 1:length(vidList)
    %������� ������ ���������������� ������ ������� ������
    procAvi = dir([name_tree(vidNum).dlum]);
    if isempty(procAvi) % ���������, ���������� �� ���� � ��������� ������ � �����
        % ���� ��� ���, ���������� �����, ��������� ���������� ������
        disp('���� � ��������� ������ �� ������')
        disp('�������� ������� ������ � ������ ����� �� ���������� �����������')
        
        [diode1lum,diode2lum] = getDiodeLum(vidList{vidNum},pnts2proc(vidNum),dpath);
        disp(['�������� ���� ' name_tree(vidNum).dlum])
        save(name_tree(vidNum).dlum,'diode1lum','diode2lum')
    else
        % ������ ������ �� �������� ������ �� �����
        disp('���������� ������� ������ ��� ���������')
        disp(['����� ���� ' procAvi.name])
        load(name_tree(vidNum).dlum)
    end
    
    % 	try
    %�������� ������� ����� behdata
    if exist(name_tree(vidNum).res,'file') == 2
        disp('�������� ���� � ������� ��� ��������.')
        disp('��� xls ����')
        %�������� ������� xls �����
        if exist(name_tree(vidNum).xls,'file') == 0
            disp('XLS ���� �� ������. ������� ��� ������� � ���������.')
            try % ���������� �������� ���� ���� ���������, ����������� xls
                calcSaccLat(name_tree(vidNum).res, dpath, vidNum)
            catch ME
                disp(ME.message)
                for ii = 1:length(ME.stack)
                    disp(['In ' ME.stack(ii).name ', line ' num2str(ME.stack(ii).line)])
                end
                continue
            end
        else
            disp('XLS ���� ��� ����. �������� � ����������.')
            continue
        end
    end
    
    % ���������� ������ ������
    % ����� ���� txt
    disp('��� ���� � ����������� ����������� ������')
    paramFile = dir(name_tree(vidNum).txt);
    %����� prt ����
    behFileName = dir(name_tree(vidNum).prt);
    
    % ��������� ��� �� ����� �������
    
    if isempty(paramFile) || isempty(behFileName)
        disp('�� ��� ����� � ������� �������')
        if isempty(paramFile)
            disp(['�� ������ ���� ' name_tree(vidNum).txt])
            disp('�������� � ���������� �����')
        else
            disp('�� ������ ���� � �������������� �������')
            disp('�������� � ���������� �����')
        end
        
        continue
        
    else
        disp('��� ������ ����� �������')
        paramFile = {paramFile.name};
        behFileName = behFileName.name;
    end
    disp(['����� ���� ' name_tree(vidNum).txt])
    [A,B,~,~,E,F,~,~,~] = getXYcoor([dpath.txt filesep paramFile{1}]); % �������� ���� � ������� �� ��������� ������
    
    disp(['����� ���� ' behFileName])
    [results, sortedDb, aDat] = beh_readFile(name_tree(vidNum).prt);
    
    % ������ ��� ������ �������
    disp('��������� ��� ������ ������')
    behData = extractSaccades(results, sortedDb, aDat, ...
        diode1lum, diode2lum, A, B, E, F);
    
    % ��� ����� ��� �������� � ��������� ������ � ������������ �������
    dThrName = [dpath.diode_thresholds filesep 'Thresholds_' ...
        num2str(name_tree(vidNum).date(1)') '-' ...
        num2str(name_tree(vidNum).date(2)') '-' ...
        num2str(name_tree(vidNum).date(3)') '_' ...
        num2str(name_tree(vidNum).date(4)') '-' ...
        num2str(name_tree(vidNum).date(5)') '-' ...
        num2str(name_tree(vidNum).date(6)') '.fig'];
    
    if ~exist(dThrName, 'file') && ishandle(248) % ����� ���, � ������� ����
        saveas(248, dThrName, 'fig')
    end
    close(248)
    
    % ��� ����� ��� �������� � ���������� ����� � ����� ���������
    eyeMovName = [dpath.diode_thresholds filesep 'EyeMovements_' ...
        num2str(name_tree(vidNum).date(1)') '-' ...
        num2str(name_tree(vidNum).date(2)') '-' ...
        num2str(name_tree(vidNum).date(3)') '_' ...
        num2str(name_tree(vidNum).date(4)') '-' ...
        num2str(name_tree(vidNum).date(5)') '-' ...
        num2str(name_tree(vidNum).date(6)') '.fig'];
    
    if ~exist(eyeMovName, 'file') && ishandle(249)
        saveas(249, eyeMovName, 'fig')
    end
    close(249)
    
    % ������ � ����������� � ������������ ������ � ���������� �����
    onOffName = [dpath.diode_thresholds filesep 'OnOffDiodes_' ...
        num2str(name_tree(vidNum).date(1)') '-' ...
        num2str(name_tree(vidNum).date(2)') '-' ...
        num2str(name_tree(vidNum).date(3)') '_' ...
        num2str(name_tree(vidNum).date(4)') '-' ...
        num2str(name_tree(vidNum).date(5)') '-' ...
        num2str(name_tree(vidNum).date(6)') '.fig'];
    
    if ~exist(onOffName, 'file') && ishandle(250)
        saveas(250, onOffName, 'fig')
    end
    close(250)
    
    % ������ � ��������� 25-�� ������ � ���������� ��������
    exampleSacc = [dpath.diode_thresholds filesep 'exampleSacc' ...
        num2str(name_tree(vidNum).date(1)') '-' ...
        num2str(name_tree(vidNum).date(2)') '-' ...
        num2str(name_tree(vidNum).date(3)') '_' ...
        num2str(name_tree(vidNum).date(4)') '-' ...
        num2str(name_tree(vidNum).date(5)') '-' ...
        num2str(name_tree(vidNum).date(6)') '.jpg'];
    
    if ~exist(onOffName, 'file') && ishandle(251)
        saveas(251, exampleSacc, 'fig')
        close(251)
    end
    
    if ~isempty(behData)
        fprintf(2, ['�������� ���� ' name_tree(vidNum).res(length(dpath.res)+2:end) '\n']);
        save (name_tree(vidNum).res, 'behData')
    end
end