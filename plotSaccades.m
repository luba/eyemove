clear all, close all

%load('K:\monk_mov\codes\file2save.mat')
%[dataFile,resDir] = uigetfile('E:/data/prosha/res/BehData_*.mat');
[dataFile,resDir] = uigetfile('E:/data/valli/res/BehData_*8-2018*.mat');
load([resDir dataFile]);

figure(155); 
grid on
unqTargets = unique(cellfun(@num2str, {behData.target},'UniformOutput',0));
currClrMap = colormap('jet');

%������� ���������� �������� ������� ��� ������� ���������� �����(35 � ������ ���
%����� � 16 ��� �����)
multiplier=1:length(unqTargets);
multiplier=round(multiplier*(64/length(unqTargets)));

hold on 
nTrials = length(behData);
for ii =  1:nTrials
    if strcmp(behData(ii).task,'NoDelay')
        %currClrInd = find(strcmp(unqTargets, num2str(behData(ii).target)))*4;
        currClrInd = multiplier(find(strcmp(unqTargets, num2str(behData(ii).target))));
        selEndFix = behData(ii).time > behData(ii).diode1off;      
        plot(behData(ii).fixCntrdSaccX(selEndFix), behData(ii).fixCntrdSaccY(selEndFix), '.',...
            'MarkerFaceColor', currClrMap(currClrInd,:),...
            'MarkerEdgeColor', currClrMap(currClrInd,:), 'MarkerSize', .5);
%         plot(behData(ii).saccadeX(selEndFix), behData(ii).saccadeY(selEndFix), '.',...
%             'MarkerFaceColor', currClrMap(currClrInd,:),...
%             'MarkerEdgeColor', currClrMap(currClrInd,:), 'MarkerSize', .5);
    end
end
hold off

figure(156); 
%unqTargets = unique(cellfun(@num2str, {behData.target},'UniformOutput',0));
currClrMap = colormap('jet');

hold on 
nTrials = length(behData);
for ii =  1:nTrials
    currClrInd = multiplier(find(strcmp(unqTargets, num2str(behData(ii).target))));
    plot(behData(ii).saccadeX, behData(ii).saccadeY, '.',...
        'MarkerFaceColor', currClrMap(currClrInd,:),...
        'MarkerEdgeColor', currClrMap(currClrInd,:), 'MarkerSize', .5);
end
hold off

figure(157); 
%unqTargets = unique(cellfun(@num2str, {behData.target},'UniformOutput',0));
currClrMap = colormap('jet');

hold on 
nTrials = length(behData);
for ii =  1:nTrials
    if strcmp(behData(ii).task,'Fix.Point')
        currClrInd = multiplier(find(strcmp(unqTargets, num2str(behData(ii).target))));
        plot(behData(ii).saccadeX, behData(ii).saccadeY, '.',...
            'MarkerFaceColor', currClrMap(currClrInd,:),...
            'MarkerEdgeColor', currClrMap(currClrInd,:), 'MarkerSize', .5);
    end
end
hold off