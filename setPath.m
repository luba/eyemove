function path = setPath(pathFile)

if exist('pathFile','var')
    if exist(pathFile,'file')
        % ���� ���� ����������, ��������� ���, ��������� � ���������� �������
        load(pathFile);
        return
    end
end
% ���� ���, ���������� ���������� �������

% ��� ����������� ����������
path.mom = uigetdir(pwd,'�������� ���������� � ������ ��������');
if path.mom == 0
    error('������� ���� � ������ AVI')
end
disp('����������� ����������:')
disp(['    ' path.mom])

% ������ ����� ����� ��������
idcs = strfind(path.mom,filesep);
path.monkey = upper(path.mom(idcs(end)+1));
disp('������ ����� ������ ��������:')
disp(['    ' path.monkey])

pathFile = [path.mom filesep 'path.mat'];

if exist(pathFile,'file')
    disp('���� � ������ ������, ��������� ��������� �� �����.')
    load(pathFile)
    return
end
disp('���������� ���� � ������')

% avi
aviDir = [path.mom filesep 'avi'];
if exist(aviDir,'dir') == 7 % ���� ���������� txt ��� ����
    path.avi = aviDir;
else
    error('��� ���������� avi. �������� �!')
end
clear aviDir
disp('���������� avi')
disp(['    ' path.avi])

% txt
txtDir = [path.mom filesep 'txt'];
if exist(txtDir,'dir') == 7 % ���� ���������� txt ��� ����
    path.txt = txtDir;
else
    error('��� ���������� txt. �������� �!')
end
clear txtDir
disp('���������� txt')
disp(['    ' path.txt])

% prt
prtDir = [path.mom filesep 'prt'];
if exist(prtDir,'dir') == 7 % ���� ���������� prt ��� ����
    path.prt = prtDir;
else
    error('��� ���������� prt. �������� �!')
end
clear prtDir
disp('���������� prt')
disp(['    ' path.prt])

% diodelum
lumDir = [path.mom filesep 'diodelum'];
if ~exist(lumDir, 'dir') % ���� ���������� diodelum �����������
    disp('���������� diodelum �����������. ������ ��.')
    mkdir(lumDir)
end
path.lum = lumDir; % � ����� ������ �������� diodelum ����������� � ��������� ������
clear lumDir
disp('���������� diodelum')
disp(['    ' path.lum])

% diodetimes
timesDir = [path.mom filesep 'diodetimes'];
if ~exist(timesDir, 'dir') % ���� ���������� diodelum �����������
    disp('���������� diodetimes �����������. ������ ��.')
    mkdir(timesDir)
end
path.times = timesDir; % � ����� ������ �������� diodelum ����������� � ��������� ������
clear lumDir
disp('���������� diodetimes')
disp(['    ' path.times])

% jpg
jpgDir = [path.mom filesep 'diodepos'];
if ~exist(jpgDir, 'dir') % ���� ���������� diodepos �����������
    disp('���������� diodepos �����������. ������ ��.')
    mkdir(jpgDir)
end
path.jpg = jpgDir; % � ����� ������ �������� diodepos ����������� � ��������� ������
clear jpgDir
disp('���������� diodepos')
disp(['    ' path.jpg])

%diode thresholds
dThr = [path.mom filesep 'diodeThresholds'];
if ~exist(dThr, 'dir') % ���� ���������� diodeThresholds �����������
    disp('���������� diodeThresholds �����������. ������ ��.')
    mkdir(dThr)
end
path.diode_thresholds = dThr; % � ����� ������ �������� diodepos ����������� � ��������� ������
clear dThr
disp('���������� diodeThresholds')
disp(['    ' path.diode_thresholds])

% behData
resDir = [path.mom filesep 'res'];
if ~exist(resDir, 'dir') % ���� ���������� res �����������
    disp('���������� res �����������. ������ ��.')
    mkdir(resDir)
end
path.res = resDir; % � ����� ������ �������� res ����������� � ��������� ������
clear resDir
disp('���������� res')
disp(['    ' path.res])

% xls
xlsDir = [path.mom filesep 'xls'];
if ~exist(xlsDir, 'dir') % ���� ���������� xls �����������
    disp('���������� xls �����������. ������ ��.')
    mkdir(xlsDir)
end
path.xls = xlsDir; % � ����� ������ �������� res ����������� � ��������� ������
clear xlsDir
disp('���������� xls')
disp(['    ' path.xls])

save(pathFile, 'path')
