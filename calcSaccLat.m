function calcSaccLat(dataFile, path, vidNum)

global name_tree

% ���������, �������������� ������ �� �������������� ������� � ������� �
% Excel

if exist('dataFile','var') == 0
    [dataFile,resDir] = uigetfile('', 'MultiSelect','on');
    
    if ~isa(dataFile,'cell')
        dataFile = {dataFile};
    end
    
    for ii = 1:length(dataFile)
        dataFile{ii} = [resDir dataFile{ii}];
    end
else
    if ~isa(dataFile,'cell')
        dataFile = {dataFile};
    end
end

for flNum = 1:length(dataFile)
    behData = [];
    TCode = [];
    load(dataFile{flNum});
    if name_tree(vidNum).monk=='V'
        dayX = datenum('27122018','ddmmyyyy'); % ���� ��������� ��� ����� - ����������!!!
    elseif name_tree(vidNum).monk=='P'
        dayX = datenum('11062019','ddmmyyyy'); % ���� ��������� ��� ����� - ����������!!!
    end
    %���������� ������� ����
    month_insert=['0' '0' num2str(name_tree(vidNum).date(2))];
    month_insert=month_insert(end-1:end);
    day_insert=['0' '0' num2str(name_tree(vidNum).date(3))];
    day_insert=day_insert(end-1:end);
    currDay = datenum([day_insert month_insert num2str(name_tree(vidNum).date(1))],'ddmmyyyy');
    
    distTrg = 5.7; % ���������� ����� ��������� �������� �� ������
    distScr = 49; % ���������� �� �������� �� ����� ���������
    deg = atan(distTrg/distScr)*180/pi;
    
    figure(155);
    set(gcf,'Position',[1 41 1280 908])
    unqTargets = unique(cellfun(@num2str, {behData.target},'UniformOutput',0));
    currClrMap = colormap('jet');
    hold on
    nTrials = length(behData);
    for ii =  1:nTrials
        if strcmp(behData(ii).task,'NoDelay')
            currClrInd = find(strcmp(unqTargets, num2str(behData(ii).target)));
            selEndFix = behData(ii).time > behData(ii).diode1off;
            %           if find(strcmp(unqTargets, num2str(behData(ii).target))) == 14
            plot(-1*behData(ii).fixCntrdSaccX(selEndFix), -1*behData(ii).fixCntrdSaccY(selEndFix), '.',...
                'MarkerFaceColor', currClrMap(currClrInd,:),...
                'MarkerEdgeColor', currClrMap(currClrInd,:), 'MarkerSize', .5);
            %         end
            %         plot(behData(ii).saccadeX(selEndFix), behData(ii).saccadeY(selEndFix), '.',...
            %             'MarkerFaceColor', currClrMap(currClrInd,:),...
            %             'MarkerEdgeColor', currClrMap(currClrInd,:), 'MarkerSize', .5);
        end
    end
    hold off
    grid on
    box on
    xlim([-50 50])
    ylim([-30 30])
    
    %[x,y] = ginput(15);
    
    for ii = 1:length(behData)
        behData(ii).target = behData(ii).target';
    end
    
    for ii = 1:25
        figure(251)
        subplot(5,5,ii)
        hold on
        
        fill([behData(ii).diode1on behData(ii).diode1on...
            behData(ii).diode1off behData(ii).diode1off],...
            [-50 50 50 -50], 'r', 'EdgeColor', 'none', ...
            'FaceAlpha',0.3)
        
        if behData(ii).diode2on > 0
            fill([behData(ii).diode2on(1) behData(ii).diode2on(1) ...
                behData(ii).diode2off(1) behData(ii).diode2off(1)], ...
                [-50 50 50 -50], 'b', 'EdgeColor', 'none', ...
                'FaceAlpha',0.3)
        end
        
        selLateFix = behData(ii).time > behData(ii).diode1off - 0.1 & behData(ii).time < behData(ii).diode1off;
        saccX = behData(ii).saccadeX - mean(behData(ii).saccadeX(selLateFix));
        saccY = behData(ii).saccadeY - mean(behData(ii).saccadeY(selLateFix));
        plot(behData(ii).time, saccX,'b');
        plot(behData(ii).time, saccY,'g');
        
        sumSacc = abs(saccX) + abs(saccY);
        plot(behData(ii).time, sumSacc,'r')
        
        df = diff(sumSacc);
        bar(behData(ii).time(2:end), df)% ,'k--'
        
        if behData(ii).diode2on > 0
            sacc_start = find(df'> 2 & behData(ii).time(2:end) > behData(ii).diode2on(1),1,'first');
            sacc_end = find(df'> 2 & behData(ii).time(2:end) > behData(ii).diode2on(1));
            sacc_end = fliplr(sacc_end);
            sacc_end = sacc_end(sacc_end < sacc_start + 10);
            sacc_end = sacc_end+1;
            if ~isempty(sacc_start)
                sacc_end = sacc_end(1);
                plot(behData(ii).time(sacc_start),sumSacc(sacc_start),'*r')
                plot(behData(ii).time(sacc_end),sumSacc(sacc_end),'*b')
                %behData(ii).latency = behData(ii).time(sacc_start) - behData(ii).diode2on(1);
                %lat(ii) = behData(ii).time(sacc(1)) - behData(ii).diode2on;
            end
        end
        title(['������� ' num2str( behData(ii).trial)])
        axis tight
        hold off
    end

    for ii = 1:length(behData)
        selLateFix = behData(ii).time > behData(ii).diode1off - 0.1 & behData(ii).time < behData(ii).diode1off;
        saccX = behData(ii).saccadeX - mean(behData(ii).saccadeX(selLateFix));
        saccY = behData(ii).saccadeY - mean(behData(ii).saccadeY(selLateFix));
    
        sumSacc = abs(saccX) + abs(saccY);
        df = diff(sumSacc);
    
        if behData(ii).diode2on(1) > 0 && ~isequal(behData(ii).target,[0; 0])
            sacc_start = find(df'> 2 & behData(ii).time(2:end) > behData(ii).diode2on(1),1,'first');
            sacc_end = find(df'> 2 & behData(ii).time(2:end) > behData(ii).diode2on(1));
            sacc_end = fliplr(sacc_end);
            sacc_end = sacc_end(sacc_end < sacc_start + 10);
            sacc_end = sacc_end+1;
            if ~isempty(sacc_start)
%             plot(behData(ii).time(sacc_start),sumSacc(sacc_start),'*r')
                behData(ii).latency = behData(ii).time(sacc_start) - behData(ii).diode2on(1); % ������ � ��������
                behData(ii).latency = 1000*behData(ii).latency; % ��������� � �����������
            
                sacc_end = sacc_end(1);
            
                behData(ii).sacc_X0 = behData(ii).saccadeX(sacc_start);
                behData(ii).sacc_X1 = behData(ii).saccadeX(sacc_end);
                behData(ii).sacc_Y0 = behData(ii).saccadeY(sacc_start);
                behData(ii).sacc_Y1 = behData(ii).saccadeY(sacc_end);
            
                behData(ii).sacc_dX = behData(ii).sacc_X1 - behData(ii).sacc_X0;
                behData(ii).sacc_dY = behData(ii).sacc_Y1 - behData(ii).sacc_Y0;
            
            else
                behData(ii).latency = NaN;
                behData(ii).sacc_X0 = NaN;
                behData(ii).sacc_X1 = NaN;
                behData(ii).sacc_Y0 = NaN;
                behData(ii).sacc_Y1 = NaN;
                behData(ii).sacc_dX = NaN;
                behData(ii).sacc_dY = NaN;
            end
        else
            behData(ii).latency = NaN;
            behData(ii).sacc_X0 = NaN;
            behData(ii).sacc_X1 = NaN;
            behData(ii).sacc_Y0 = NaN;
            behData(ii).sacc_Y1 = NaN;
            behData(ii).sacc_dX = NaN;
            behData(ii).sacc_dY = NaN;
        end
    end

    selList = ~isnan([behData.latency]);

    Date = repmat({datestr(currDay,'dd.mm.yyyy')},sum(selList),1);

    target = [behData.target];

    H = target(1,selList)';
    DegH = deg*H;
    DegH = mat2cell(DegH,ones(sum(selList),1),1);

    H = mat2cell(H,ones(sum(selList),1),1);

    V = target(2,selList)';
    DegV = deg*V;
    DegV = mat2cell(DegV,ones(sum(selList),1),1);

    V = mat2cell(V,ones(sum(selList),1),1);

    X0 = {behData(selList).sacc_X0}';

    X1 = {behData(selList).sacc_X1}';

    Y0 = {behData(selList).sacc_Y0}';

    Y1 = {behData(selList).sacc_Y1}';

    latency = {behData(selList).latency}';

    dX = {behData(selList).sacc_dX}';

    dY = {behData(selList).sacc_dY}';
    
    Paradigm = {behData(selList).task}';
    
    for ii = 1:length(behData)
        currTask = behData(ii).task;
        if strcmp(currTask, 'GAP')
            Paradigm2{ii} = 1;
        elseif strcmp(currTask, 'NoDelay')
            Paradigm2{ii} = 2;
        elseif strcmp(currTask, 'Overlap')
            Paradigm2{ii} = 3;
        end
    end
    Paradigm2 = Paradigm2(selList)';
    
% amp = ([behData(selList).sacc_dX].^2 + [behData(selList).sacc_dY].^2).^0.5;
% amp = mat2cell(amp',ones(sum(selList),1),1);
    amp = cell(sum(selList),1);

    for ii = 1:length(behData)
        TCode(ii) = HV2TCode(behData(ii).target);
    end

    TCode = TCode(selList)';
    TCode = mat2cell(TCode,ones(sum(selList),1),1);
%TCode = cell(sum(selList),1); % ������������, ��� � ��������

    Day = currDay - dayX;%cell(sum(selList),1);
    Day = repmat(Day,sum(selList),1);
    Day = mat2cell(Day,ones(sum(selList),1),1);

    header = {'Date','DegH','DegV','H','V','X0','X1','Y0','Y1','Latency (ms)', ...
        'dX','dY','Amp (deg)','TCode','Day','Paradigm','Paradigm2'};
    allData = [Date, DegH, DegV, H, V, X0, X1, Y0, Y1, latency, ...
        dX, dY, amp, TCode, Day, Paradigm, Paradigm2];
    allData = [header; allData];

    xlsFolder = path.xls;
    if ~exist(xlsFolder,'file')
        mkdir(xlsFolder)
    end

%     xlsName = [xlsFolder filesep 'SaccadeData_' dateStr '.xls'];
%     disp(['�������� XLS ���� SaccadeData_' dateStr '.xls'])
%     xlswrite(xlsName,allData)
    xlsName = name_tree(vidNum).xls;
    disp(['�������� XLS ���� ' xlsName])
    xlswrite(xlsName,allData)

    latencies = [behData.latency];

    unqTrg = unique([behData([behData.latency] ~= 0).target]','rows');

    targets = [behData.target]';
    targets = mat2cell(targets,ones(1,length(behData)),2);

%     figure,
%     set(gcf,'Position',[1 41 1280 908])
%     for trg = unqTrg'
%         if ~isequal(trg,[0; 0])
%             a = cellfun(@(x) isequal(x,trg'), targets); % ������� ����������� ������� �� ������ ������
%     
%             xpos = trg(1);
%             ypos = -1*trg(2); % ������������� ����������, ����� ���������� ��� ������� ������� ���� ������
%             nSbp = 7*(ypos + 2) + (xpos + 4);
%     
%             subplot(5,7,nSbp)
%             currLat = latencies(a);
%             currLat(currLat == 0) = [];
%     
%             xLat = 0:25:500;
%             latBar = histc(currLat,xLat);
%     
%             bar(xLat,latBar)
%             hold on
%             plot(nanmedian(currLat),0,'r*')
%             I = iqr(currLat);
%             plot(-1,I,'wo')
%             hold off
%             title(['[' num2str(trg') ']'])
%             med = num2str(nanmedian(currLat));
%             med = med(1:5);
%             I = num2str(I);
%             I = I(1:5);
%             text(0.2,15,{['N=' num2str(sum(~isnan(currLat)))]; ...
%                 ['Med=' med]; ...
%                 ['IQR=' I]}, ...
%                 'FontSize',8)
% %         leg = legend({['N=' num2str(sum(~isnan(currLat)))]; ...
% %             ['Med=' med]; ...
% %             ['IQR=' I]}, ...
% %             'FontSize', 8); % 'Orientation', 'horizontal', ... 'Location', 'EastOutside'
%             xlim([0 500])
%             ylim([0 21])
%             set(gca,'XTick',0:100:500,'XTickLabel',0:100:500, ...
%                 'YTick',0:5:20,'YTickLabel',0:5:20)
%             if isequal(trg,[-3; 2])
%                 xlabel('�����������, �')
%                 ylabel('����� ������')
%             end
%         end
%     end
%     close all
end