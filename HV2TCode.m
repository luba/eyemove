function [TCode] = HV2TCode(target)
% ��������� �������������� �������� ���� ��������� ���� �� ������ �
% �������� TCode, ������ ��� ����������� ������ �� �������� � Excel.
% 
% ������ �������������:
% [TCode] = HV2TCode(target);
% 
% target - ������������ ������� �� ���� ��������

empt = find(target == 0); % ����� ������� �������� -> �������������� ��� ������������ ������

if empt == 1 % ���� ������ ������������
    if target(2) == -2
        TCode = 7;
    elseif target(2) == -1
        TCode = 8;
    elseif target(2) == 1
        TCode = 9;
    elseif target(2) == 2
        TCode = 10;
    end
elseif empt == 2 % ���� ������ ��������������
    if target(1) == -3
        TCode = 1;
    elseif target(1) == -2
        TCode = 2;
    elseif target(1) == -1
        TCode = 3;
    elseif target(1) == 1
        TCode = 4;
    elseif target(1) == 2
        TCode = 5;
    elseif target(1) == 3
        TCode = 6;
    end
elseif ~isequal(empt,[1;2]) % ���� ��������� ������ ��������� � �� X, � �� Y
    if isequal(target,[-3;2])
        TCode = 11;
    elseif isequal(target,[-3;-2])
        TCode = 12;
    elseif isequal(target,[3;2])
        TCode = 13;
    elseif isequal(target,[3;-2])
        TCode = 14;
    else
        TCode = 0;
    end
else
    TCode = 0;
end
% 	target codes	TCode
% 	(H, V)	
% Horizontal	-3	1
% 	-2	2
% 	-1	3
% 	1	4
% 	2	5
% 	3	6
% Vertical	-2	7
% 	-1	8
% 	1	9
% 	2	10
% Oblique	(-3, 2)	11
% 	(-3, -2)	12
% 	(3, 2)	13
% 	(3, -2)	14

