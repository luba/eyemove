function [pnts2proc] = getDiodePos(vidList, dpath)

global name_tree

% ���������� ������ Matlab
v = version('-release');

for vidNum = 1:length(vidList)
%     % ��������� ��� �������� ����� JPG
%     undScrId = find(vidList{vidNum}{1} == '_',2,'last');
%     dotId = find(vidList{vidNum}{1} == '.');
%     picName = [path.jpg filesep 'Diodepos_' vidList{vidNum}{1}(undScrId(1)+1:dotId(1)-1) '.jpg'];
%     % ��������� ��� �������� �����, ���� ���������� ��������� ������
%     structName = [path.jpg filesep 'Diodepos_' vidList{vidNum}{1}(undScrId(1)+1:dotId(1)-1) '.mat'];
    picName=name_tree(vidNum).dpos_jpg;
    structName=name_tree(vidNum).dpos;
    if exist(picName,'file')
        % ���� ����������� ����������, ���������, ���������� �� ���� ��
        % ����������
        if exist(structName,'file')
            % ���� ���� ����������, ��������� ������ �� ���� � ���������
            pnts2proc(vidNum) = load(structName);
            continue % ������������� �� ��������� ����
        else
            % ���� ���, ���������� ���������� ���������, ����� �������� ���
            % ������ � �� ���������
        end
    else
        % ���� ���, ���������� ���������� ���������, ����� �������� ���
        % ������ � �� ���������
    end
    
    % ������������ ������ ������� ��� ������ � ����� � ������ ������� Matlab
    if strcmp(v,'2011a') || strcmp(v,'2009a')% Older Matlab
        movObj = VideoReader([dpath.avi filesep vidList{vidNum}{1}]);
        firstFrame = read(movObj,1);
    elseif strcmp(v,'2014b') % Matlab R2014b
        movObj = VideoReader([dpath.avi filesep vidList{vidNum}{1}]);
        firstFrame = readFrame(movObj);
    else
        error('������ ������ Matlab �� ��������������')
    end

    % ������������� ����� � ������ ��� ����������� �����
    selFrames = single(zeros(size(firstFrame,1),size(firstFrame,2),50));

    if strcmp(v,'2011a') || strcmp(v,'2009a')% Older Matlab
        frNums = ceil(linspace(1,movObj.NumberOfFrames,50));
        for ii = 1:50
            currFrame = read(movObj,frNums(ii));
            currFrame = single(currFrame);
            currFrame = mean(currFrame,3);
            selFrames(:,:,ii) = currFrame;
        end
    elseif strcmp(v,'2014b') % Matlab R2014b
        %nFrames = floor(movObj.Duration * movObj.FrameRate);
        frDurs = linspace(0,movObj.Duration-1,50);
        for ii = 1:50
            movObj.CurrentTime = frDurs(ii);%(ii - 1)*1000/movObj.FrameRate;
            currFrame = readFrame(movObj);
            currFrame = single(currFrame);
            currFrame = mean(currFrame,3);
            selFrames(:,:,ii) = currFrame;
        end
    % movObj = VideoReader([dataDir vidList{1}]);
    % firstFrame = readFrame(movObj);
    else
        error('������ ������ Matlab �� ��������������')
    end

    frame2show = mean(selFrames,3);

    figure(250)
    imagesc(frame2show)
    colormap gray
    axis image off
    
    hold on
    rectangle('Position',[10 10 size(frame2show,2)-20 size(frame2show,1)-20], ...
        'Curvature',[0,0],'EdgeColor',[1 1 1])
    
    %specify 1st diode center coordinates
    set(250, 'NumberTitle', 'off', ...
        'Name', '������� ������� ���� ������ ���� (����. ���; ��� �������, �������)');
    [d1_x,d1_y] = getpts(250); % ����� ����� �� ������������
    d1_x = int16(round(d1_x));
    d1_y = int16(round(d1_y));
    nPntsDiod = 10;
    pnts2proc(vidNum).d1x = d1_x-nPntsDiod:d1_x+nPntsDiod;
    pnts2proc(vidNum).d1y = d1_y-nPntsDiod:d1_y+nPntsDiod;

    %specify 2nd diode center coordinates
    set(250, 'NumberTitle', 'off', ...
        'Name', '������� ������� ���� �������� ������ ���� (�����. ����.; ��� �������, ������)');
    [d2_x,d2_y] = getpts(250);
    d2_x = int16(round(d2_x));
    d2_y = int16(round(d2_y));
    pnts2proc(vidNum).d2x = d2_x-nPntsDiod:d2_x+nPntsDiod;
    pnts2proc(vidNum).d2y = d2_y-nPntsDiod:d2_y+nPntsDiod;
    
    plot(d1_x, d1_y, 'ro')
    text(double(d1_x - 100), double(d1_y),'Diode 1','Color','r')
    plot(d2_x, d2_y, 'bo')
    text(double(d2_x - 100), double(d2_y),'Diode 2','Color','b')
    hold off
    
    % ��������� ����������� � ��������� ������
    saveas(250, picName)
    
    % ��������� ��������� � ��������� ���������
    d1x = pnts2proc(vidNum).d1x;
    d1y = pnts2proc(vidNum).d1y;
    d2x = pnts2proc(vidNum).d2x;
    d2y = pnts2proc(vidNum).d2y;
    save(structName,'d1x','d1y','d2x','d2y')
    
    close(250) % ������� �����������, ����� ���������� ����� ��������, ������ ����� ���������
    delete(movObj) % ������� ������
    clear movObj selFrames frame2show
end