function [A,B,C,D,E,F,G,H,I] = getXYcoor(paramFile)
[A,B,C,D,E,F,G,H,I] = ...
    textread(paramFile,'%u %f %u %u %f %f %f %f %f');
end