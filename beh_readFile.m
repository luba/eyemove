function [results, sortedDb, aDat] = beh_readFile(fName)

global dPath

% [fname, fpath] = uigetfile ([dPath 'VTR*.PRT'], ...
%     '�������� ���� � �������������� �������');
% fName = sprintf ('%s%s', fpath, fname);

fprintf('\nProcessing file -> %s\n',fName);

fileID = fopen(fName,'r');
A = fscanf(fileID,'%c');
fclose(fileID);

% ������� ������ �� ���������
% ����� ��� �����
header(1).paramName = 'File';
tmp_id = strfind(A,header(1).paramName);
lineEndId = find(A == char(10),1,'first');
tmp = A(tmp_id + length(header(1).paramName) + 1:lineEndId-1);
lastSlash = find(tmp == '\',1,'last');
tmp = tmp(lastSlash+1:end-1);
header(1).paramStr = tmp;
header(1).paramNum = header(1).paramStr;

% ����� ��������� ����
header(2).paramName = 'Background';
tmp_id = strfind(A,header(2).paramName);
lineEndId = find(A == char(10));
lineEndId = lineEndId(lineEndId > tmp_id);
lineEndId = lineEndId(1);
tmp = A(tmp_id + length(header(2).paramName) + 1:lineEndId-1);
header(2).paramStr = str2num(tmp);
header(2).paramNum = header(2).paramStr;

% ��������� ������������ �����
header(3).paramName = 'Fixation point';
tmp_id = strfind(A,header(3).paramName);
tmp_id = tmp_id(1);
lineEndId = find(A == char(10));
lineEndId = lineEndId(lineEndId > tmp_id);
lineEndId = lineEndId(1);
tmp = A(tmp_id + length(header(3).paramName) + 1:lineEndId-1);
header(3).paramStr = str2num(tmp);
header(3).paramNum = header(3).paramStr;

% ��������� ������
header(4).paramName = 'Normal target';
tmp_id = strfind(A,header(4).paramName);
tmp_id = tmp_id(1);
lineEndId = find(A == char(10));
lineEndId = lineEndId(lineEndId > tmp_id);
lineEndId = lineEndId(1);
tmp = A(tmp_id + length(header(4).paramName) + 1:lineEndId-1);
header(4).paramStr = str2num(tmp);
header(4).paramNum = header(4).paramStr;

% ��������� ��������� ������
header(5).paramName = 'Dimed target';
tmp_id = strfind(A,header(5).paramName);
tmp_id = tmp_id(1);
lineEndId = find(A == char(10));
lineEndId = lineEndId(lineEndId > tmp_id);
lineEndId = lineEndId(1);
tmp = A(tmp_id + length(header(5).paramName) + 1:lineEndId-1);
header(5).paramStr = str2num(tmp);
header(5).paramNum = header(5).paramStr;

% ������ ������������ �����
header(6).paramName = 'Fixation point size';
tmp_id = strfind(A,header(6).paramName);
tmp_id = tmp_id(1);
lineEndId = find(A == char(10));
lineEndId = lineEndId(lineEndId > tmp_id);
lineEndId = lineEndId(1);
tmp = A(tmp_id + length(header(6).paramName) + 1:lineEndId-1);
tmp(ismember(tmp, ' pixel')) = ' ';
header(6).paramStr = tmp;
header(6).paramNum = str2num(header(6).paramStr);

% ������ ������
header(7).paramName = 'Target size';
tmp_id = strfind(A,header(7).paramName);
tmp_id = tmp_id(1);
lineEndId = find(A == char(10));
lineEndId = lineEndId(lineEndId > tmp_id);
lineEndId = lineEndId(1);
tmp = A(tmp_id + length(header(7).paramName) + 1:lineEndId-1);
tmp(ismember(tmp, ' pixel')) = ' ';
header(7).paramStr = tmp;
header(7).paramNum = str2num(header(7).paramStr);

% ��������� �������� �� �����������
header(8).paramName = 'Stimul appearance';
tmp_id = strfind(A,header(8).paramName);
lineEndId = find(A == char(10));
lineEndId = lineEndId(lineEndId > tmp_id);
lineEndId1 = lineEndId(1);
lineEndId2 = lineEndId(2);
tmp = A(tmp_id + length(header(8).paramName) + 1:lineEndId1-1);
tmp(ismember(tmp, ' stimulsinpercentsofscreenwidth')) = ' ';
header(8).paramStr = tmp;
header(8).paramNum = str2num(tmp);

% ��������� �������� �� ���������
header(9).paramName = 'Stimul appearance';
tmp = A(lineEndId1+1:lineEndId2-1);
tmp(ismember(tmp, ' stimulsinpercentsofscreenwidth')) = ' ';
header(9).paramStr = tmp;
header(9).paramNum = str2num(tmp);

% ����������� ����� �����
header(11).paramName = 'Tasks were presented in proportion';
tmp_id = strfind(A,header(11).paramName);
lineEndId = find(A == char(10));
lineEndId = lineEndId(lineEndId > tmp_id);
lineEndId1 = lineEndId(1);
lineEndId2 = lineEndId(2);
tmp = A(tmp_id + length(header(11).paramName) + 1:lineEndId1 - 1);
header(11).paramStr = tmp;
tmp(tmp == ':') = ' ';
header(11).paramStr = tmp;
header(11).paramNum = str2num(tmp);

% ���� �����
header(12).paramName = 'Tasks';
tmp = A(lineEndId1+1:lineEndId2-1);
tmp(tmp == ' ' | tmp == '(' | tmp == ')') = '';
header(12).paramNum = header(11).paramNum;

% ����� �������
header(13).paramName = 'Channels';
tmp_id = strfind(A,header(13).paramName);
lineEndId = find(A == char(10));
lineEndId = lineEndId(lineEndId > tmp_id);
lineEndId = lineEndId(1);
tmp = A(tmp_id + length(header(13).paramName) + 2:lineEndId-1);
tmp(ismember(tmp, ' channel(s)wereregistered')) = ' ';
header(13).paramNum = str2num(tmp);

% ���� ������������
header(14).paramName = 'Date';
tmp_id = strfind(A,header(14).paramName);
startId = strfind(A,'Start time:');
tmp = A(tmp_id + length(header(14).paramName) + 2:startId-1);
tmp(tmp == ' ') = '';
header(14).paramStr = tmp;
header(14).paramNum = datevec(header(14).paramStr,  'dd.mm.yyyy');

% ����� ������������
header(15).paramName = 'Start time';
lineEndId = find(A == char(10));
lineEndId = lineEndId(lineEndId > tmp_id);
lineEndId = lineEndId(1);
tmp = A(startId + length(header(15).paramName) + 2:lineEndId-1);
header(15).paramStr = tmp;
header(15).paramNum = str2num(header(15).paramStr(end-4:end));

% Finish time:
% Experiment duration:

lHeader = length(header);
for hh = 1:lHeader
    fprintf('\n%s ->\t', header(hh).paramName);
    for kk = 1:length(header(hh).paramNum)
        fprintf('%d\t', header(hh).paramNum(kk));            
    end
end
fprintf('\n');
startId = strfind(A,'Start time:'); %����� ������ ������� ������ �� �������������� 'Start time:'
startId = startId + 20;

endId = strfind(A,'Finish time:');%����� ����� ������� ������ �� �������������� 'Finish time:'
endId = endId - 1;

tbl = A(startId:endId);%�������� ������ �� �������, ��������� header

%�������� ��� 'No Delay' ������
noDelId = strfind(tbl,'No Delay');
tbl(noDelId+2) = [];
clear noDelId

%�������� ��� 'no reAct' ������
noReactId = strfind(tbl,'no reAct');
tbl(noReactId+2) = [];
clear noReactId

C = textscan(tbl,'%f %s %s %f %f %f %f %f %f %f');
clear tbl

% newLineID = strfind(tbl,sprintf('\n'));
% endHeader = find(newLineID > startId, 'first')

%aDat = importdata(fName, ' ', 13);
%aDat = importdata(fName, ' ', 14);

aDat.data = [C{end-6:end}];

[nTrials, nParams] = size(aDat.data);

results = {};
sortedDb = {};
    

fprintf('\n')

results.header = header;
%results.rDataTasks = {behData.task};
[results.rDataTasks] = C{2}';
%results.rDataResp = {behData.response};
[results.rDataResp] = C{3}';
results.rDataTimes = [C{4:10}];
    
%clear behData

unqTasks = unique([results.rDataTasks]);
unqResps = unique([results.rDataResp]);

nTasks = length(unqTasks);
nResps = length(unqResps);
nExps = length(results);

for ii = 1:nTasks 
    sortedDb(ii).task = unqTasks{ii};
    sortedDb(ii).resps = unqResps;    
    sortedDb(ii).respMatr = zeros(nExps,nResps);    
    sortedDb(ii).rtMatr = zeros(nExps,nResps);    
    for jj = 1:nExps
        if sum(strcmp(results(jj).rDataTasks, unqTasks{ii})) > 0
            sortedDb(ii).expList{jj} = results(jj).header(1).paramNum;    
            fprintf('\n%s', results(jj).header(1).paramNum);  
            fprintf('\t%s', unqTasks{ii}); 
            %currSelList = [];
            for kk = 1:nResps
                currSelList = strcmp(results(jj).rDataResp, unqResps{kk}) & strcmp(results(jj).rDataTasks, unqTasks{ii});
                if sum(currSelList) > 0
                    % currRespsMatr(kk) = sum(currSelList);
                    sortedDb(ii).respMatr(jj,kk) = sum(currSelList);
                    sortedDb(ii).rtMatr(jj,kk) = mean(results(jj).rDataTimes(currSelList,4));
                    fprintf('\t%s:%d RT: %1.2f', unqResps{kk}, sortedDb(ii).respMatr(jj,kk), sortedDb(ii).rtMatr(jj,kk));
                end
            end
            fprintf('\n')
        end
    end
end