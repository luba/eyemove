function exploreVideo
global dataDir
global movObj

dataDir = '../data/video/Valli/';
[vFname,dataDir] = uigetfile([dataDir '.avi']);

movObj = VideoReader([dataDir vFname]);
firstFrame = readFrame(movObj);

figure(250)
imagesc(firstFrame)
axis image off

end