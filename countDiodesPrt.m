clear all, close all

dpath = setPath;

diodeList = dir([dpath.lum filesep '*.mat']);

data = cell(length(diodeList), 6);

% loop through mat-files
for flNum = 1:length(diodeList)
    
    currDate = diodeList(flNum).name(10:19);
    data{flNum, 1} = currDate;
    
    currYear = diodeList(flNum).name(10:13);
    currMonth = diodeList(flNum).name(15:16);
    currDay = diodeList(flNum).name(18:19);
    
    prtName = [dpath.prt filesep currYear filesep ...
        dpath.monkey 'TR' currMonth currDay '.PRT'];
    
    if exist(prtName, 'file')
        [diode1num, diode2num] = getDiodeOnOff([dpath.lum filesep diodeList(flNum).name]);
        data{flNum, 2} = diode1num;
        data{flNum, 3} = diode2num;
        [results, sortedDb, aDat] = beh_readFile(prtName);
        data{flNum, 4} = length(results.rDataTasks);
        if diode1num == length(results.rDataTasks)
            data{flNum, 5} = 1;
        else
            data{flNum, 5} = 0;
        end
        data{flNum, 6} = diode1num - length(results.rDataTasks);
    end
end
